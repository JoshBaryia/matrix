var express 	= require('express');
var app		= express();



app.get('/:a/:b', function(req,res){

	// get the dimension /a/b

	var xdim = req.url.split('/')[1];
	var ydim = req.url.split('/')[2];

	res.writeHead(200);
	
	if(validatePath(req.url)){
		matrix = createMatrix(xdim, ydim);
		drawMatrix(matrix);
		var sp = spiral(matrix,xdim,ydim);
		res.write("<p> " + sp + "</p>");
	}else{
		res.write("Sorry, please enter a valid path!");
	}

	function validatePath(path){
	
		var xval 	= false;
		var yval 	= false;	
	
		var x 		= path.split("/")[1];
		var y 		= path.split("/")[2];
	
		for(var i = 1; i < 20; i++){

			if(x == i){
				xval = true;
			}

			if(y == i){
				yval = true;
			}

			if(yval == true && xval == true) return true;
		
		}
		return false;

	}


	function drawMatrix(m){
			
		res.write("\n\n");	

		for(var i = 0; i < m.length; i++){
			for(var j = 0; j < m[0].length; j++){
				res.write(m[i][j] + "\t");				
			}
			res.write("\n");
		}

	}

	function spiral(m,xd,yd){
	
		res.write("\n\n");

		var l 		= 0;
		var lim 	= (xd > yd)? xd : yd;
		var i, 
		    j;
		var toReturn 	= "";

		
		while(l < lim/2){
			for(j = l; j < yd - l - 1; j++){
				toReturn += m[l][j].toString() + " ";
			}
	
			for(i = l; i < xd - l - 1; i++ ){
				toReturn += m[i][yd - l - 1].toString() + " ";
			
			}
			
			for(j = yd - l - 1; j > l; j--){
				toReturn += m[xd - l - 1][j].toString() + " ";
			}

			for(i = xd - l - 1; i > l; i--){
				toReturn += m[l][l].toString() + " ";			
			}
			l++;
		}
		return toReturn;
	}


	res.end();

}).listen(3000);


function createMatrix(x,y){

	var count 	= 1;
	var mat 	= new Array(y);

	for(var i = 0; i < x; i++){
		mat[i]	= new Array(y);
		for(var j = 0; j < y; j++){
			mat[i][j] = count++;
		}
	}
	return mat;
}







